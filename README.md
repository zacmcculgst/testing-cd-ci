# **NOTE: This is a PUBLIC repo. Do not push any code that is restricted in any way.**
## Testing CD-CI
Trying to develop a proper CD/CI pipeline so that it can be implemented in full on the project, very experimental and likely perpetually broken.

![Pipeline](CD_CI_Chart.svg?raw=true)

## Proposed CD/CI pipeline
Upon pushing a commit to a branch in the gitlab repository, auto devops as part of gitlab will run the .gitlab-ci.yml configuration and will build a docker image with docker installed on it (dind), as well as install gradle and java. Then a gradlew build command will be issued which will cause gradle to build the compiled language applications (Java for now), generate the proto files required (using gradle grpc plugin), and then call the docker-compose file (using gradle-docker-compose plugin). The docker-compose will then create the required docker images for each microservice, each of which installing and creating their appropriate environments. Additionally as part of the dockerfiles, they will run their discrete unit testing and static code analysis, flake8 and unittest for python, SpotBugs and junit for Java. The procedure can be roughly seen sketched above.

## Steps taken so far (roughly)
1. Created gradle project in Eclipse, then pushed local to git repo in new branch. Need to include gradle wrapper, gradlew.
1. (optional?) installed eclipse docker tooling plugin with WSL2 (if you're doing this, make sure you have latest windows 10 update)
1. Install gitlab-runner (haven't used yet)
1. Enabled shared runners for gitlab, then register with the gitlab runner with following instructions: https://docs.gitlab.com/runner/register/index.html (note also make sure to install). For shared runners, use https:// one of the URLS with the random string above as the token
1. Chmod the gradlew file and commit
1. Select the correct docker image and configure 
1. Configure and set up docker-compose file
1. Set build.gradle to call docker-compose

## Outstanding Issues:
1. Why is only the pb2.py being generated, not the grpc_pb2.py ?
1. Flake8 really wants to run on lib python files, not only ones in dir
1. Why is gradle trying to used a cached exe on linux?


Note: Some sample code taken from https://github.com/grpc/grpc/blob/master for gRPC testing purposes