FROM gradle:latest
USER root
LABEL maintainer="zmccullough@gst.com"
RUN apt-get update &&\
    apt-get -y install python3.8 &&\
    apt-get -y install python3-pip &&\
    mkdir /wigs
RUN pip3 install grpcio-tools
COPY . /wigs
RUN ls -la /wigs/proto/ &&\
    chmod +x /wigs/proto/create.sh
#RUN /wigs/proto/create.sh
RUN ["python3", "-m", "grpc_tools.protoc", "-I/wigs/proto/", "--python_out=/wigs/python/", "--grpc_python_out=/wigs/python/", "/wigs/proto/helloworld.proto"]