#!/bin/sh
mkdir ../python
apt-get update
apt-get install python3
apt-get install python3-pip
pip3 install grpcio-tools
#mkdir protobuf_python
#cd protobuf_python || exit
#touch __init__.py
#python3 -m grpc_tools.protoc -I../../proto/ --python_out=. --grpc_python_out=. ../../proto/*.proto
python3 -m grpc_tools.protoc -I. --python_out=../python/ --grpc_python_out=../python/ ./*.proto